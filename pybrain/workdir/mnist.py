from numpy import ravel

from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer

def classify(training, testing, HIDDEN_NEURONS, MOMENTUM
                WEIGHTDECAY, LEARNING_RATE_DECAY, EPOCHS):
    trndata = ClassificationDataSet(INPUT_FEATURES, 1, nb_classes=CLASSES)
    tstdata = ClassificationDataSet(INPUT_FEATURES, 1, nb_classes=CLASSES)

    for i in range(len(testing)):
        tstdata.addSample(ravel(testing['x'][i]),
                                [testing['y'][i]])
    for i in range(len(trndata)):
        trndata.addSample(ravel(trndata['x'][i]),
                                [trndata['y'][i]])

    trndata._convertToOneMany()
    tstdata._convertToOneMany()

    fnn = buildNetwork(trndata.indim, HIDDEN_NEURONS, trndata.outdim,
                        outclass=SoftmaxLayer)

    trainer = BackpropTrainer(fnn, dataset=trndata, momentum=MOMENTUM,
                              verbose=True, weightdecay=WEIGHTDECAY,
                              learningrate=LEARNING_RATE,
                              lrdecay=LEARNING_RATE_DECAY)

    for i in range(EPOCHS):
        trainer.trainEpochs(1)
        trnresult = percentError(trainer.testOnClassData(),
                                 trndata['class'])
        tstresult = percentError(trainer.testOnClassData(dataset = tstdata),
                                 tstdata['class'])

        print("epoch: %4d" % trainer.totalepochs,
              " train error: %5.2f%%" % trnresult,
              " test error: %5.2f%%" % tstresult)
    return fnn
