import keras
from keras.datasets import mnist
from keras.models import model_from_yaml
from keras.utils import np_utils
import numpy

numpy.random.seed(42)

(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784)

X_train = X_train.astype('float32')

y_train = np_utils.to_categorical(y_train, 10)

yaml_file = open("mnist_model.yml", "r")
loaded_model_yaml = yaml_file.read()
yaml_file.close()

model = model_from_yaml(loaded_model_yaml)
model.load_weights("mnist_model.h5")

model.compile(loss="categorical_crossentropy", optimizer="ADAM",
                metrics=["accuracy"])

print(model.summary())

model.fit(X_train, y_train, batch_size=400, nb_epoch=10,
                    validation_split=0.2, verbose=1)

model_yaml = model.to_yaml()
yaml_file = open("mnist_model.yml", "w")

yaml_file.write(model_yaml)
yaml_file.close()

model.save_weights("mnist_model.h5")
