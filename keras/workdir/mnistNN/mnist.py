import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils
from keras.datasets import mnist
from keras.models import model_from_yaml

numpy.random.seed(42)

(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784)

X_train = X_train.astype('float32')

y_train = np_utils.to_categorical(y_train, 10)

model = Sequential()

model.add(Dense(800, input_dim=784, activation="relu", kernel_initializer="normal"))
model.add(Dense(100, activation="relu", kernel_initializer="normal"))
model.add(Dense(10, activation="softmax", kernel_initializer="normal"))

model.compile(loss="categorical_crossentropy", optimizer="ADAM", metrics=["accuracy"])

print(model.summary())

model.fit(X_train, y_train, batch_size=400, nb_epoch=50, validation_split=0.2, verbose=1)

model_yaml = model.to_yaml()
yaml_file = open("mnist_model.yml", "w")

yaml_file.write(model_yaml)
yaml_file.close()

model.save_weights("mnist_model.h5")
