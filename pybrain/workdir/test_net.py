from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure import TanhLayer

net = buildNetwork(2, 3, 1, bias=True, hiddenclass=TanhLayer)
net.activate([2, 1])

ds = SupervisedDataSet(2, 1)

ds.addSample((0, 0), (0,))
ds.addSample((0, 1), (1,))
ds.addSample((1, 0), (1,))
ds.addSample((1, 1), (0,))

len(ds)

for inpt, target in ds:
    print inpt, target

trainer = BackpropTrainer(net, ds)

trainer.train()
