# To start docker
$ service docker start 
## In folder with docker-compose.yml
$ sudo docker-compose up






# To start container
## All available containers
$ sudo docker ps
## Choose a container which you want to work with
$ sudo docker exec -it {image_name} /bin/bash
