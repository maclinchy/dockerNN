import numpy as np
from keras.utils import np_utils
from keras.datasets import mnist
from keras.models import model_from_yaml
from keras.preprocessing import image

img_path = '5.3.png'
img = image.load_img(img_path, grayscale=True)

x = image.img_to_array(img)
x = x.reshape(1, 784)

x = 255 - x
x /= 255
x = np.expand_dims(x, axis=0)
x = x[0]

yaml_file = open("mnist_model.yml", "r")
loaded_model_yaml = yaml_file.read()
yaml_file.close()

loaded_model = model_from_yaml(loaded_model_yaml)
loaded_model.load_weights("mnist_model.h5")
loaded_model.compile(loss="categorical_crossentropy",
                    optimizer="SGD", metrics=["accuracy"])

prediction = loaded_model.predict(x)
print("Prediction: ")
print(np.argmax(prediction))
